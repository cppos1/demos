// snapcast client

#include "sound.hh"

#include "dbgOutHelpers.hh"
#include "sysHelpers.hh"

#include "cppos/net/iocontext.hh"

#include <rp2040-scheduler.hh>
#include <cppos/evqueue.hh>
#include "i2s.hh"
#include <uart.hh>
#include "gpio.hh"
#include "timer.hh"

#include "rp2040.hh"

#include <execution>
#include <chrono>
#include <thread>
#include <charconv>

#include <array>
#include <cstdint>
#include <string_view>
#include <atomic>

#include "decode.hh"

#include "cppos/scheduler.tt"


using namespace std::literals;

cppos::EvQueueBody<SnapCast::WireChunk, inBufSize> inBuf;
cppos::EvQueueBody<PcmFrame, outBufSize> outBuf;


#define LINT(x) (doLog && (dbgInt(__LINE__ + 10000, (x)), true))

void netAudioStart(::nstd::net::io_context &io);
void startHttpd(::nstd::net::io_context &io);

volatile uint32_t irqCnt = 0;
volatile uint32_t lastSize = 0;
volatile uint32_t i2sIrqCnt = 0;
volatile uint32_t i2sIrqPkt = 0;


namespace
{

constexpr uint32_t i2sDataPin = 19;
constexpr uint32_t i2sBClkPin = 20;
// unused, as fixed by PIO program
//constexpr uint32_t i2sLRClkPin = i2sBClkPin + 1;

constexpr AudioConfig outSettings = { .sampleFreqHz = 48000, };
constexpr AudioI2sConfig i2sSettings =
{
    .dataOutPin = i2sDataPin,
    .bClkPin = i2sBClkPin,
};


constexpr uint32_t PeriFreq = 126'000'000;
constexpr uint32_t BaudRate = 115200;
constexpr uint32_t txPin = 8;
constexpr uint32_t rxPin = 9;


constexpr int printBufSize = 180;
char printBuf[printBufSize] = "Here's the board\r\n";

//typedef cppos::preempt_context<0> MyExecT;
typedef cppos::exec_context MyExecT0;
typedef cppos::single_context<1> MyExecT1;

alignas(4) uint8_t execCtx0Buf[sizeof(MyExecT0) + 40];
alignas(4) uint8_t execCtx1Buf[sizeof(MyExecT1) + 40];
MyExecT0 *ctx0 = nullptr;
MyExecT1 *ctx1 = nullptr;

::nstd::net::io_context io;

void netTask()
{
    myprintf("=====");

    netAudioStart(io);
    startHttpd(io);

    myprintf("starting network...");
    dbgOut.flush();

    io.run();
}

} // unnamed namespace


// global function for I2S driver

bool pullPcmFrame(PcmFrame &fr)
{
    ++i2sIrqCnt;
    // this runs inside the ISR for I2S on core0
    cppos::EvQueuePull<PcmFrame, outBufSize, MyExecT0> pcmQ(outBuf);

    if (pcmQ.try_pop(fr) == cppos::queue_op_status::empty)
    {
        return false;
    }
    ++i2sIrqPkt;

    return true;
}


int main()
{
    system_setup();

    syncDbgOut(printBuf, 18);

    i2sSetup(&i2sSettings, &outSettings);
    i2sStart();

    namespace exec = std::execution;
    ctx0 = new (execCtx0Buf) MyExecT0;
    ctx1 = new (execCtx1Buf) MyExecT1;

    exec::execute(ctx0->scheduler<1, 4440>(), [] { netTask(); });

    // opus decoder requires a pretty big stack
    exec::execute(ctx1->scheduler<3, 6800>(), [] { decode(); });

    // Start everything
    cppos::startContexts();

    /* we should never get here! */
    return 0;
}
