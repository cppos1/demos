#ifndef DECODE_HH_INCLUDED
#define DECODE_HH_INCLUDED

#include "sound.hh"

#include <atomic>

constexpr int channels = 2;

extern std::atomic<uint32_t> opusPacketCount;
[[noreturn]] void decode();
#endif /* DECODE_HH_INCLUDED */
