// networking part of Snap client

#include "sound.hh"

#include "dbgOutHelpers.hh"

#include <cppos/coSender.hh>

#include "picowi.hh"
#include "lwip.hh"
#include "cppos/net/stdnet.hh"
#include "cppos/net/opHolder.hh"
#include "cppos/net/iocontext.hh"

#include "nstd/net/ip/tcp.hpp"
#include "nstd/net/basic_stream_socket.hpp"
#include "nstd/net/basic_socket_acceptor.hpp"
#include "nstd/net/ip/basic_endpoint.hpp"
#include "nstd/net/async_accept.hpp"
#include "nstd/net/async_receive.hpp"
#include "nstd/execution.hpp"

#include <array>


namespace
{

namespace EX = ::nstd::execution;
namespace NN = ::nstd::net;
namespace NI = ::nstd::net::ip;

using endpoint = NI::basic_endpoint<NI::tcp>;

constexpr NI::address_v4 myIp(NI::address_v4::bytes_type{192, 168, 8, 15});
constexpr NI::address_v4 myMask(NI::address_v4::bytes_type{255, 255, 255, 0});

constexpr NI::address_v4::bytes_type srvAddr{192, 168, 8, 6};
constexpr uint16_t snapCastPort = 1704;

constexpr size_t netBufSize = 1000;
std::array<std::byte, netBufSize> netBuf;

typedef cppos::exec_context MyExecT0; // we run on core0
cppos::EvQueuePush<SnapCast::WireChunk, inBufSize, MyExecT0> opusQ(inBuf);


class AudioServer
{
public:
    AudioServer(NI::address_v4 addr, uint16_t port)
      : sock(NI::tcp::v4())
    {
        endpoint ep(addr, port);
        sock.connect(ep);

        auto t = std::chrono::steady_clock::now().time_since_epoch();
        auto uSec = std::chrono::duration_cast<std::chrono::microseconds>(t).count();

        SnapCast::BaseAligned baseMsg =
        {
            0,
            {
                SnapCast::hello,
                0, 0,
                {int32_t(uSec / 1'000'000), int32_t(uSec % 1'000'000)},
                {0, 0},
                SnapCast::helloMsg.size + uint32_t(sizeof(SnapCast::helloMsg.size)),
            },
        };

        sock.send(NN::buffer(&baseMsg.msg, sizeof(baseMsg.msg)));
        sock.send(NN::buffer(&SnapCast::helloMsg, baseMsg.msg.size));

        // read and discard everything until we get a CodecHeader
        do
        {
            sock.receive(NN::buffer(&baseMsg.msg, sizeof(baseMsg.msg)));
            sock.receive(NN::buffer(netBuf.data(), baseMsg.msg.size));
        } while (baseMsg.msg.type != SnapCast::codecHeader);

        myprintf("Network handshake done");
    }

    void start(NN::io_context &io)
    {
        auto loop =
            [this, &io] () -> cppos::CoSender
            {
                while (true)
                {
                    NN::mutable_buffer buf = NN::buffer(&baseMsg.msg, sizeof(baseMsg.msg));

                    do
                    {
                        auto [n] =  co_await EX::on(io.scheduler(),
                                                    NN::async_receive(sock, buf));
                        buf += n;
                    } while (buf.size() != 0);

                    if (baseMsg.msg.type == SnapCast::wireChunk)
                    {
                        buf = NN::buffer(&chunk, baseMsg.msg.size);
                    }
                    else
                    {
                        buf = NN::buffer(netBuf.data(), baseMsg.msg.size);
                    }

                    do
                    {
                        auto [n] =  co_await EX::on(io.scheduler(),
                                                    NN::async_receive(sock, buf));
                        buf += n;
                    } while (buf.size() != 0);

                    if (baseMsg.msg.type == SnapCast::wireChunk)
                    {
                        opusQ.try_push(chunk);
                    }
                }
            };

        EX::start_detached(loop());

    }

private:
    NI::tcp::socket sock;

    SnapCast::BaseAligned baseMsg;
    SnapCast::WireChunk chunk;
    NN::mutable_buffer curBuf;
};

std::byte audioStore[sizeof(AudioServer)];

} // unnamed namespace


void netAudioStart(NN::io_context &io)
{
    wifiInit();
    netInit(std::byteswap(myIp.to_uint()), std::byteswap(myMask.to_uint()));

    auto srv = new (audioStore) AudioServer(srvAddr, snapCastPort);
    srv->start(io);
}
