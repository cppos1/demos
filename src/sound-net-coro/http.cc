// networking part of Snap client

#include "sound.hh"
#include "decode.hh"

#include "dbgOutHelpers.hh"

#include <cppos/coSender.hh>

#include "picowi.hh"
#include "lwip.hh"
#include "cppos/net/stdnet.hh"
#include "cppos/net/opHolder.hh"
#include "cppos/net/iocontext.hh"

#include "nstd/net/ip/tcp.hpp"
#include "nstd/net/basic_stream_socket.hpp"
#include "nstd/net/basic_socket_acceptor.hpp"
#include "nstd/net/ip/basic_endpoint.hpp"
#include "nstd/net/async_accept.hpp"
#include "nstd/net/async_receive.hpp"
#include "nstd/execution.hpp"

#include <array>
#include <charconv>


namespace
{

namespace EX = ::nstd::execution;
namespace NN = ::nstd::net;
namespace NI = ::nstd::net::ip;

using endpoint = NI::basic_endpoint<NI::tcp>;

constexpr std::string_view notFoundReply =
    "HTTP/1.0 404 Not Found\r\n"
    "\r\n"
    "<html>"
    "<head><title>Not Found</title></head>"
    "<body><h1>404 Not Found</h1></body>"
    "</html>";

constexpr std::string_view okHdr =
    "HTTP/1.0 200 OK\r\n"
    "Content-Type: text/html\r\n"
    "\r\n";

constexpr std::string_view htmlStart =
    "<html>"
    "<head><title>Opus Packet Counter</title></head>"
    "<body><h1>Opus Packet Counter</h1>"
    "<p>Current count:";

constexpr std::string_view htmlEnd =
    "</p></body></html>";

constexpr size_t netBufSize = 1000;
std::array<char, netBufSize> netBuf;

alignas(4) uint8_t httpSrvStorage[sizeof(NI::tcp::acceptor)];

NI::tcp::acceptor *httpServer;

size_t makeHttpReply(size_t n)
{
    //we expect something like "GET path HTTP/1.1"
    auto &npos = std::string_view::npos;

    std::string_view req(netBuf.data(), n);

    auto pos = req.find("GET");
    if (pos == npos)
    {
        return notFoundReply.copy(netBuf.data(), netBufSize);
    }

    pos = req.find_first_not_of(' ', pos + 3);
    if (pos == npos)
    {
        return notFoundReply.copy(netBuf.data(), netBufSize);
    }

    pos = req.find("/noOfOpusPkts", pos);
    if (pos == npos)
    {
        return notFoundReply.copy(netBuf.data(), netBufSize);
    }

    pos = 0;
    pos = okHdr.copy(netBuf.data(), netBufSize);
    pos += htmlStart.copy(netBuf.data() + pos, netBufSize - pos);
    auto bufEnd = netBuf.data() + netBufSize;
    auto end = std::to_chars(netBuf.data() + pos, bufEnd,
                             opusPacketCount.load()).ptr;
    end += htmlEnd.copy(end, bufEnd - end);

    return end - netBuf.data();
}
} // unnamed namespace


void startHttpd(NN::io_context &io)
{
    myprintf("Initializing HTTPD");

    httpServer = new (httpSrvStorage) NI::tcp::acceptor(endpoint(NI::address_v4::any(), 80));

    auto clientLoop =
        [&] () -> cppos::CoSender
        {
            while (true)
            {
                myprintf("Starting HTTPD");

                auto [client, ep] = co_await EX::on(io.scheduler(),
                                                    NN::async_accept(*httpServer));
                myprintf("Client connected");

                while (true)
                {
                    auto [n] = co_await EX::on(io.scheduler(),
                                               NN::async_read_some(client, NN::buffer(netBuf)));

                    if (n == 0) break;

                    n = makeHttpReply(n);
                    co_await EX::on(io.scheduler(),
                                    NN::async_write_some(client, NN::buffer(netBuf, n)));
                }
            }

        };

    myprintf("Pre HTTP");
    EX::start_detached(clientLoop());
    myprintf("Post HTTP");
}
