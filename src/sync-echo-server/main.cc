// simple synchronous TCP echo server

#include "dbgOutHelpers.hh"
#include "sysHelpers.hh"

#include "picowi.hh"
#include "lwip.hh"

#include <cppos/scheduler.hh>
#include <cppos/net/iocontext.hh>
#include "cppos/net/stdnet.hh"

#include <execution>
#include <bit>

#include "nstd/net/ip/basic_endpoint.hpp"
#include "nstd/net/basic_socket_acceptor.hpp"
#include "nstd/net/basic_stream_socket.hpp"
#include "nstd/net/ip/address_v4.hpp"

#include <cppos/scheduler.tt>


#define LINT(x) (doLog && (dbgInt(__LINE__ + 10000, (x)), true))


using namespace std::literals;

namespace
{
namespace NN = ::nstd::net;
namespace NI = ::nstd::net::ip;
namespace EX = ::nstd::execution;

using socket_acceptor = NN::basic_socket_acceptor<NI::tcp>;
using stream_socket = NN::basic_stream_socket<NI::tcp>;
using endpoint = NI::basic_endpoint<NI::tcp>;

constexpr NI::address_v4 myIp(NI::address_v4::bytes_type{192, 168, 1, 15});
constexpr NI::address_v4 myMask(NI::address_v4::bytes_type{255, 255, 255, 0});

std::byte netBuf[256];

NN::io_context io;

alignas(4) uint8_t execCtxBuf[sizeof(cppos::exec_context) + 40];


void netTask()
{
    myprintf("=====");
    wifiInit();
    netInit(std::byteswap(myIp.to_uint()), std::byteswap(myMask.to_uint()));

    socket_acceptor server(endpoint(NI::address_v4::any(), 12345));

    while (true)
    {
        stream_socket client{server.accept()};

        myprintf("Client connected");

        while (true)
        {
            size_t n = client.read_some(NN::buffer(netBuf));
            myprintf("Bytes got:");
            LINT(n);

            if (n == 0) break;

            client.write_some(NN::buffer(netBuf, n));
        }

        myprintf("Client done");
    }
}

cppos::exec_context *ctx = nullptr;
} // unnamed namespace


int main( void )
{
    system_setup();

    namespace exec = std::execution;
    ctx = new (execCtxBuf) cppos::exec_context;

    // the picowi lib puts a 2k buffer on the stack!
    // (SPI_MSG in wifi_data_read)
    exec::execute(cppos::scheduler<1, 4440>(ctx), [] { netTask(); });

    // Start everything
    ctx->start();

    /* we should never get here! */
    return 0;
}
