cmake_minimum_required(VERSION 3.24)

include(../base.cmake)

# our project is C++ only, but PicoWi and lwIP are C
project(sync-tcp-server C CXX)

set(DEMO_SRCS
  main.cc
  ${PICO_LIB_DIR}/picowi/picowi.cc
  ${PICO_LIB_DIR}/uart.cc
  ${PICO_LIB_DIR}/timer.cc
  ${PICO_LIB_DIR}/rp2040.cc
  ${PICO_LIB_DIR}/sysHelpers.cc
  ${PICO_LIB_DIR}/dbgOutHelpers.cc
)

include_directories(${CMAKE_CURRENT_LIST_DIR})

include(../common.cmake)
