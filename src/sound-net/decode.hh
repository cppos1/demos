#ifndef DECODE_HH_INCLUDED
#define DECODE_HH_INCLUDED

#include "sound.hh"

constexpr int channels = 2;

[[noreturn]] void decode();
#endif /* DECODE_HH_INCLUDED */
