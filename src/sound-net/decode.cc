
#include "decode.hh"

#include "sound.hh"

//#include "dbgOutHelpers.hh"

#include <rp2040-scheduler.hh>
#include <cppos/evqueue.hh>
#include "uart.hh"
#include "timer.hh"

#include "opus_config.h"
#include <opus.h>

#include <chrono>
#include <thread>
#include <array>
#include <charconv>
#include <cassert>

#define USE_OPUS 1
#include <silk/control.h>

using namespace std::literals;

// OPUS memory
extern char *global_stack;
extern char *global_stack_end;
extern char *scratch_ptr;

#define LINT(x) uart1Int(__LINE__ + 20000, (x))

namespace
{
constexpr uint32_t sampleRate = 48000;

// server setting is 20ms, but the server may have some leeway
constexpr int maxFrameLenMs = 25;
constexpr int maxFrameSamples = maxFrameLenMs * (sampleRate / 1000);

// this runs on core1
typedef cppos::single_context<1> MyExecT;

uint32_t dbgCntDecode;

//std::array<opus_int16, maxFrameSamples * 4> decodeBuf;

std::array<char, GLOBAL_STACK_SIZE> opusStack;

// opus_decoder_get_size returned 26496
constexpr size_t opusDecoderSize = 26496;
std::array<std::byte, opusDecoderSize> decoderBuf;

SnapCast::WireChunk opusBuf;
PcmFrame pcmBuf;

constexpr int printBuf2Size = 60;
char printBuf2[printBuf2Size] = "Here's the board\r\n";

void uart1Int(int line, unsigned val)
{
    auto bufEnd = printBuf2 + printBuf2Size;
    char *end = printBuf2;

    end = std::to_chars(end, bufEnd, line).ptr;
    *(end++) = ':';
    end = std::to_chars(end, bufEnd, val, 16).ptr;
    *(end++) = '\r';
    *(end++) = '\n';

    while (!uart1.done())
    {
        //std::this_thread::sleep_for(1ms);
        __NOP();
    }
    uart1.write(printBuf2, end - printBuf2);
}

} // unnamed namespace

[[noreturn]] void decode()
{
    cppos::EvQueuePull<SnapCast::WireChunk, inBufSize, MyExecT> opusQ(inBuf);
    cppos::EvQueuePush<PcmFrame, outBufSize, MyExecT> pcmQ(outBuf);
    uart1.write("decoder started\r\n", 17);

    // we need this inside the function to get the interrupt
    // on our core
    rp2040::Timer iniWait(1);

    size_t opusSize = opus_decoder_get_size(2);
    LINT(opusSize);
    assert(opusSize >= opusDecoderSize);

    OpusDecoder *decoder = std::bit_cast<OpusDecoder *>(decoderBuf.data());

    global_stack = opusStack.data();
    scratch_ptr = global_stack;
    global_stack_end = global_stack + GLOBAL_STACK_SIZE;

    int err = opus_decoder_init(decoder, sampleRate, channels);
    assert(err == OPUS_OK);

    // we wait until the ring buffer is filled to about half
    opusQ.wait_pop(opusBuf); // we skip the first packet
    iniWait.waitForUs<MyExecT>((inBufSize / 2) * 20'000);
    uart1.write("Starting decode...\r\n", 20);

    int chunkCnt = 0;
    while (true)
    {
        opusQ.wait_pop(opusBuf);
        ++chunkCnt;

#if USE_OPUS
        auto src = reinterpret_cast<const uint8_t *>(opusBuf.payload);
        size_t len = std::min(sizeof(opusBuf.payload), size_t(opusBuf.size));
        auto dest = reinterpret_cast<int16_t *>(pcmBuf.samples);
        int samples = opus_decode(decoder,
                                  src, len,
                                  dest, maxSamplesPerFrame,
                                  0);

        assert(samples >= 0);

        pcmBuf.sampleCnt = samples;
        pcmQ.wait_push(pcmBuf);
#endif

        ++dbgCntDecode;
        if ((dbgCntDecode & 0x1ff) == 1)
        {
            LINT(chunkCnt);
        }
    }
}
