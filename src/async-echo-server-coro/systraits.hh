#ifndef SYSTRAITS_HH_INCLUDED
#define SYSTRAITS_HH_INCLUDED

#include <cppos/defaultSysTraits.hh>

namespace cppos
{
struct sys_traits : default_sys_traits
{
    static constexpr uint8_t max_event_count = 32;
};

struct port_traits
{
    // the scheduler doesn't need to know about the time slice
    // it's the port that need to handle this
    static constexpr int time_slice_ms = 1;
    static constexpr uint32_t sys_timer_value = 126'000;
    // for now we need this :-(
    static constexpr void(*sys_timer_setup)() = nullptr;
    // the SDK should define which one to use
    // I've seen an ADA implementation that uses 31, but I think
    // it should be one of the reserved ones
    static constexpr uint32_t atomic_spinlock_no = 5;
    static constexpr uint32_t used_cores = 1;
};

struct board_traits
{
    static constexpr bool useUart1 = false;
};
} // namespace cppos
#endif /* SYSTRAITS_HH_INCLUDED */
