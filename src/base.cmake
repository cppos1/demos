# either a directory with env.cmake must be defined as ENV_DIR
# or these must be set:
#  PICO_SDK_PATH
#  CMSIS_ROOT
#  CPPOS_DIR
#  PICO_LIB_DIR
#  PICO_BIN_DIR
#  LWIP_DIR
#  OPUS_DIR
#  PICOWI_DIR
#  KUHL_DIR

if(DEFINED ENV_DIR)
  include(${ENV_DIR}/env.cmake)
endif()

# root to search for Pico SDK and CMSIS
set(CMAKE_FIND_ROOT_PATH ${CMSIS_ROOT} ${PICO_SDK_PATH})

# set the target board
set(BOARD rp-pico)

set(CMAKE_BUILD_TYPE Debug)

# initialize C++OS build
include(${CPPOS_DIR}/cmake/use-cppos.cmake)

# our chip does have SYSTICK
set(CPPOS_TICK USE_SYSTICK)


set (LWIP_INCLUDE_DIRS
    "${TOP_DIR}"
    "${PICO_LIB_DIR}/lwip"
    "${LWIP_DIR}/src/include"
)

# our project is C++ only, but PicoWi and lwIP are C
project(pico-apps C CXX)

add_compile_options(-g -finline-functions -fno-omit-frame-pointer)

add_compile_definitions(CPPOS)

add_link_options(-Wl,--just-symbols=${PICO_LIB_DIR}/picowi/cyw43-firmware.syms)
