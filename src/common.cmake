# LwIP

# our lwIP glue
include(${CPPOS_DIR}/support/lwip/c++os-lwip.cmake)
set(CPPOS_LWIP_LIBNAME c++OsLwIp)
target_include_directories(${CPPOS_LWIP_LIBNAME} PRIVATE ${PICO_LIB_DIR}/picowi)

# our network support
include(${CPPOS_DIR}/src/net/c++os-net.cmake)

include(${LWIP_DIR}/src/Filelists.cmake)
target_include_directories(lwipcore PRIVATE ${PICO_LIB_DIR}/lwip)

# for simplicity all our apps are called 'demo'
add_executable(demo ${DEMO_SRCS})

# as link order is important, we always link I2S, even if we don't need it
# the linker will not use any of it if not required
target_link_libraries(demo
  c++OsNet
  ${PICO_BIN_DIR}/libpicowi.a
  ${PICO_BIN_DIR}/libi2s.a
  ${PICO_BIN_DIR}/librp2040sdk.a
  ${CPPOS_LWIP_LIBNAME}
  lwipcore
)

if(USE_OPUS)
add_compile_definitions(
  HAVE_CONFIG_H
  GLOBAL_STACK_SIZE=40000
  CPPOS
)

add_subdirectory(${PICO_LIB_DIR}/opus opus)
target_link_libraries(demo opus)
endif()

# add C++OS binaries
use_cppos(demo)

# create the .lst, .bin and .hex files
cppos_add_extra_outputs(demo)

target_include_directories(demo PRIVATE ${PICO_LIB_DIR})
target_include_directories(demo PRIVATE ${PICO_LIB_DIR}/picowi)
target_include_directories(demo PRIVATE ${PICO_LIB_DIR}/lwip)
target_include_directories(demo PRIVATE ${PICO_LIB_DIR}/sdk-dummy)
target_include_directories(demo PRIVATE ${CPPOS_DIR}/support/lwip)
target_include_directories(demo PRIVATE ${KUHL_DIR}/src ${LWIP_INCLUDE_DIRS})
