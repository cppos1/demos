cmake_minimum_required(VERSION 3.24)

# either a directory with env.cmake must be defined as ENV_DIR
# or these must be set:
#  PICO_SDK_PATH
#  CPPOS_DIR
if(DEFINED ENV_DIR)
  include(${ENV_DIR}/env.cmake)
endif()

set(LDSCRIPT ${CMAKE_CURRENT_LIST_DIR}/fw.ld)

# even if we don't use C++OS, we still use the settings
# toolchain settings
set(CMAKE_CXX_COMPILER_FORCED TRUE)
set(CMAKE_C_COMPILER_FORCED TRUE)

set(CMAKE_TOOLCHAIN_FILE ${CPPOS_DIR}/cmake/cm0+-toolchain.cmake)
include(${CPPOS_DIR}/cmake/use-cppos.cmake)

function(cppos_extra_board_setup TARGET)
  # nothing to do
endfunction()

project(cyw43-firmware C)

add_compile_options(-ffunction-sections -fdata-sections)

add_link_options(-Wl,--cref,--gc-sections,--no-warn-mismatch)
add_link_options(-nostdlib -nostartfiles -T ${LDSCRIPT})

add_executable(cyw43-firmware ${PICOWI_DIR}/firmware/fw_43439.c)

cppos_add_extra_outputs(cyw43-firmware)
