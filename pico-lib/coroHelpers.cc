// allocation functions for coroutines

#include <cppos/coSender.hh>

#include <bit>

namespace
{
// for coroutine allocation we assume that the same frames are
// repeatedly allocated
// so we use an exact fit only algorithm

struct MemInfo
{
    MemInfo *next;
    size_t size;
};

constexpr size_t coroBufSize = 1000;
alignas(4) std::byte memPool[coroBufSize];

MemInfo *memInit()
{
    auto base = std::bit_cast<MemInfo *>(&(memPool[0]));
    base->next = nullptr;
    base->size = coroBufSize;

    return base;
}
MemInfo *freelist = memInit();

} // unnamed namespace

namespace cppos
{
void* CoSender::promise_type::operator new(size_t size) noexcept
{
    size += sizeof(MemInfo);

    MemInfo *prev = nullptr;
    MemInfo *p = freelist;
    for (MemInfo **ref = &freelist;
         p != nullptr;
         prev = p, p = p->next, ref = &p)
    {
        // we have an exact fit
        if (p->size == size)
        {
            if (ref)
            {
                *ref = p->next;
            }
            else
            {
                freelist = p->next;
            }

            return p + 1;
        }
    }

    if (p == nullptr && prev->size < size)
    {
        return nullptr;
    }

    // we need to split the last free block
    prev->size -=size;
    auto newAddr = std::bit_cast<std::byte *>(prev) + size;
    auto newBlock = std::bit_cast<MemInfo *>(newAddr);

    newBlock->size = size;
    newBlock->next = nullptr;
    return newBlock + 1;
}

void CoSender::promise_type::operator delete(void *p) noexcept
{
    auto freeBlock = static_cast<MemInfo *>(p) - 1;
    freeBlock->next = freelist;
    freelist = freeBlock;
}

} // namespace cppos
