// extremely simple ring buffer to be used for serial drivers
#ifndef RING_HH_SEEN_
#define RING_HH_SEEN_

#include <cstdint>
#include <cstddef>
#include <atomic>

constexpr bool isPowerOf2(unsigned int n)
{
    if (n == 0)
    {
        return false;
    }
    return (n & (n-1)) == 0;
}

template <unsigned int Size>
class RingBuffer
{
public:
    RingBuffer()
    {
        static_assert(isPowerOf2(Size), "Size must be power of two");
        static_assert(Size <= 256, "We only support Size up to 256");
    }

    bool empty() const
    {
        return curSize == 0;
    }

    // it looks like GCC creates for ARM CM0 wrong code (returns always 0)
    // if the return type is uint8_t!
    // not a GCC bug, but mine!
    // 256 as uint8_t is 0!
    uint16_t free() const
    {
        return Size - curSize;
    }

    void put(uint8_t c)
    {
        data[tail] = c;
        ++curSize;
        ++tail;
        tail &= Size - 1;
    }

    uint8_t get()
    {
        uint8_t c = data[head];
        --curSize;
        ++head;
        head &= Size - 1;

        return c;
    }

private:
    volatile uint8_t head = 0;
    volatile uint8_t tail = 0;
    std::atomic<uint16_t> curSize = 0;

    volatile uint8_t data[Size];
};
#endif // RING_HH_SEEN_
