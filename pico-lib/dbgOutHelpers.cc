// helper functions for debug printing

#include "dbgOutHelpers.hh"

#include <uart.hh>

#include <cstring>
#include <charconv>

namespace
{

constexpr int printBufSize = 180;
char printBuf[printBufSize] = "Here's the board\r\n";
} // unnamed namespace

void syncDbgOut(char const *str, size_t len)
{
    while (!dbgOut.done())
    {
        //std::this_thread::sleep_for(1ms);
        __NOP();
    }
    dbgOut.write(str, len);
}

extern "C"
{
[[noreturn]] void error_handler();

int doLog = 1;

void myprintf(char const *fmt)
{
    char *end = stpncpy(printBuf, fmt, printBufSize-2);
    *(end++) = '\r';
    *(end++) = '\n';

    syncDbgOut(printBuf, end - printBuf);
}

void __assert_func(char const *file, int line, char const *func,
                   char const *expr)
{
    auto bufEnd = printBuf + printBufSize;

    char *end = stpncpy(printBuf, file, printBufSize - 2);
    *(end++) = ':';
    end = stpncpy(end, func, (end - printBuf) - 2);
    *(end++) = ':';
    end = std::to_chars(end, bufEnd, line).ptr;
    *(end++) = ':';
    end = stpncpy(end, expr, (end - printBuf) - 2);
    *(end++) = '\r';
    *(end++) = '\n';

    syncDbgOut(printBuf, end - printBuf);
    dbgOut.flush();

    abort();
}

void dbgInt(int line, unsigned val)
{
    auto bufEnd = printBuf + printBufSize;
    char *end = printBuf;

    end = std::to_chars(end, bufEnd, TIMER->TIMERAWL, 16).ptr;
    *(end++) = ':';
    end = std::to_chars(end, bufEnd, line).ptr;
    *(end++) = ':';
    end = std::to_chars(end, bufEnd, val, 16).ptr;
    *(end++) = '\r';
    *(end++) = '\n';
    syncDbgOut(printBuf, end - printBuf);
}

[[noreturn]] void fatal(char const *msg)
{
    myprintf(msg);
    dbgOut.flush();
    error_handler();
}
} // extern "C"
