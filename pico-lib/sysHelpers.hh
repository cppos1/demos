#ifndef SYS_HELPERS_HH_INCLUDED
#define SYS_HELPERS_HH_INCLUDED

extern "C" [[noreturn]] void error_handler();
void system_setup();

#endif /* SYS_HELPERS_HH_INCLUDED */
