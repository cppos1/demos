#ifndef RP2040_HH_SEEN
#define RP2040_HH_SEEN

#include <rp2040.h>


namespace rp2040
{

void clocks_init();
void start_core1(void (*)());
inline uint32_t timeUs()
{
    return TIMER->TIMERAWL;
}
} // namespace rp2040
#endif /* RP2040_HH_SEEN */
