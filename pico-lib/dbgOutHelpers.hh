#ifndef DBG_OUT_HELPERS_HH_INCLUDED
#define DBG_OUT_HELPERS_HH_INCLUDED

#include <cstddef>


void syncDbgOut(char const *str, size_t len);

extern "C"
{
extern int doLog;

void myprintf(char const *fmt);
void __assert_func(char const *file, int line, char const *func,
                   char const *expr);
void dbgInt(int line, unsigned val);

[[noreturn]] void fatal(char const *msg);
} // extern "C"

#endif /* DBG_OUT_HELPERS_HH_INCLUDED */
