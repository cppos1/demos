// general helpers for C++OS on RP2040

#include "sysHelpers.hh"

#include "rp2040.hh"
#include "helpers.hh"
#include <port-header.hh>

extern "C"
{
[[gnu::weak]] void SystemInit()
{
    rp2040::clocks_init();

    // we enable the pins here, as we're going to need them anyways
    enablePeripheral(RESETS_RESET_io_bank0_Msk | RESETS_RESET_pads_bank0_Msk);

    // we need the PIOs and DMA
    enablePeripheral(RESETS_RESET_pio0_Msk | RESETS_RESET_pio1_Msk);
    enablePeripheral(RESETS_RESET_dma_Msk | RESETS_RESET_busctrl_Msk);

    /*
     * Missing:
     *
     * - ROM functions
     *   - possibly use pico_mem_ops and pico_bit_ops from SDK
     * - spinlocks
     */
}

[[noreturn]] void error_handler()
{
    //cppos::debug_handle();
    __BKPT(0);

    while (true)
    {
    }
}
} // extern "C"

void system_setup()
{
    cppos::port::disable_interrupts();

    // on reset all spinlocks are locked
    // unlock them
    constexpr static Reg slReg{SIO_SPINLOCK0};
    for (int i = 0; i != 32; ++i)
    {
        slReg.ptr()[i] = 1;
    }
}
