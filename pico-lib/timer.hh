#ifndef RP2040_TIMER_HH_INCLUDED
#define RP2040_TIMER_HH_INCLUDED

#include "rp2040.hh"

#include <cppos/scheduler.hh>
#include <cppos/coro.hh>

#include <cstdint>

namespace rp2040
{

// proof of concept only

class Timer : public cppos::event
{
public:
    template <class EvLoop>
    class Awaitable
    {
        typedef std::coroutine_handle<> coroHandleT;

    public:
        Awaitable(Timer &t_, EvLoop &el)
          : t(t_)
          , loop(el)
        {
        }

        ~Awaitable()
        {
            if (coro)
            {
                loop.remove(t, coro);
            }
        }

        bool await_ready() noexcept
        {
            return t.wakeupTime <= timeUs();
        }

        bool await_suspend(coroHandleT self) noexcept
        {
            if (!await_ready())
            {
                coro = self;

                loop.add(t, coro);
                return true; // stay suspended
            }
            else
            {
                return false;
            }
        }

        void await_resume() noexcept
        {
            loop.remove(t, coro);
            coro = nullptr;
        }


    private:
        Timer &t;
        EvLoop &loop;
        coroHandleT coro;
    };

    explicit Timer(uint32_t timerNo);
    ~Timer();

    template <typename Exec>
    void waitForUs(uint32_t timeoutUs)
    {
        Exec::preBlock(*this);
        // timeoutUs == 0: wait until wrap
        setup(timeoutUs);

        Exec::finalBlock(*this);
    }

    template <typename EvLoop>
    Awaitable<EvLoop> coWaitForUs(EvLoop &loop, uint32_t timeoutUs)
    {
        EvLoop::executor_type::blockEvent(*this);
        // timeoutUs == 0: wait until wrap
        setup(timeoutUs);

        return {*this, loop};
    }

    static void fire(uint32_t timerNo);

    void setup(uint32_t timeout);

private:
    uint32_t instance;
    uint32_t wakeupTime;

    static cppos::event *events[4];
};
} // namespace rp2040
#endif /* RP2040_TIMER_HH_INCLUDED */
