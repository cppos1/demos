#ifndef SNAP_CAST_HH_INCLUDED
#define SNAP_CAST_HH_INCLUDED

#include <cstdint>
#include <cstddef>

namespace SnapCast
{
constexpr size_t maxOpusFrameSize = 180;

// the protocol is little-endian and we're little-endian
enum MsgType : uint16_t
{
    base = 0,
    codecHeader = 1,
    wireChunk = 2,
    serverSettings = 3,
    time = 4,
    hello = 5,
    streamTags = 6,
};

struct [[gnu::packed]] TimeStruct
{
    int32_t sec;
    int32_t uSec;
};

// !!! be aware of the misalignment of the 32-bit values !!!
struct [[gnu::packed]] Base
{
    MsgType type;
    uint16_t id;
    uint16_t refersTo;
    TimeStruct sent;
    TimeStruct recv;
    uint32_t size;
};
// for aligned access
struct [[gnu::packed]] BaseAligned
{
    uint16_t padding;
    Base msg;
};

struct [[gnu::packed]] Hello
{
    uint32_t size;
    char jsonString[1024];
};

struct [[gnu::packed]] WireChunk
{
    TimeStruct stamp;
    uint32_t size;
    std::byte payload[maxOpusFrameSize]; // actually `size`
};

//constexpr char helloStr[] =
#define HELLO_STR \
    R"({"Arch":"demo",)" \
    R"("ClientName":"Soundclient",)" \
    R"("HostName":"picowspkr",)" \
    R"("ID":"00:11:22:33:44:55",)" \
    R"("Instance":1,)" \
    R"("MAC":"00:11:22:33:44:55",)" \
    R"("OS":"C++OS",)" \
    R"("SnapStreamProtocolVersion": 2,)" \
    R"("Version": "0.0.1"})"

#if 1
inline Hello helloMsg =
{
    sizeof(HELLO_STR) - 1, // no null-byte
    HELLO_STR,
};
#else
inline Hello helloMsg =
{
    .size = sizeof(HELLO_STR) - 1, // no null-byte
    .jsonString = HELLO_STR,
};
#endif
} // namespace SnapCast
#endif /* SNAP_CAST_HH_INCLUDED */
