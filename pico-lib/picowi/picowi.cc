
#include "picowi.hh"

#include "lwipExt.hh"

#include <picowi_defs.h>
#include "picowi_pico.h"
#include "picowi_wifi.h"
#include "picowi_init.h"
#include "picowi_ioctl.h"
#include "picowi_event.h"
#include "picowi_join.h"
//#include "picowi_scan.h"

#include <cstdint>
#include <thread> // for sleep
#include <chrono>

#include "systraits.hh"
#include <cppos/scheduler.hh>
#include <timer.hh>

#include "local.hh"


// picowi defs

// definitions from picowi
#define MAXFRAME    1500        /* Maximum frame size (excl CRC) */

extern "C"
{

extern uint8_t my_mac[6];

// from lwIP, but we call it from netLoop
void sys_check_timeouts();

uint32_t pico_ticks_us()
{
    return TIMER->TIMERAWL;
}
} // extern "C"

// to be called for net ETH packets
void processNetPacket(void const *data, size_t len);

rp2040::Timer netTimer(0);

namespace
{
using namespace std::literals;

//constexpr int logMask = DISP_INFO | DISP_IOCTL;
//constexpr int logMask = DISP_INFO;
constexpr int logMask = 0;


extern "C" int eth_event_handler(EVENT_INFO *eip)
{
    if (not (eip->chan == SDPCM_CHAN_DATA && eip->dlen > 0))
    {
        return 0;
    }

    processNetPacket(eip->data, eip->dlen);

    return 1;
}
} // unnamed namespace

uint8_t *getEthMac()
{
    return my_mac;
}

EvSetType netLoop(EvSetType evs)
{
    auto resultSet = evs & cppos::event::getUnblocked();
    if (resultSet.any())
    {
        return resultSet;
    }

    while (true)
    {
        int ret = event_poll();

        resultSet = evs & cppos::event::getUnblocked();
        if (resultSet.any())
        {
            return resultSet;
        }

        // to reconnect automatically
        join_state_poll(wfSsid, wfPw);

        // we had something, check if we have more
        if (ret > 0) continue;

        // now we should wait for an interrupt and use select()
        // but we have two interrupts, one for the CYW43 and one for the DMA
        // we first have to figure out how to deal with this
        // for now we just poll...
        std::this_thread::sleep_for(1ms);

        // for now we run it here
        // we should setup a timer for this
        sys_check_timeouts();
    }
}

void ethSend(void const *data, size_t len)
{
    event_net_tx(const_cast<void *>(data), len);
}

void wifiInit()
{
    // initialize PicoWi driver
    set_display_mode(logMask);
    io_init();
    wifi_setup();

    if (!wifi_init())
    {
        myprintf("Cyw43 init failed");
        error_handler();
    }

    add_event_handler(eth_event_handler);
    add_event_handler(join_event_handler);

#if 0
    add_event_handler(scan_event_handler);
    if (!scan_start())
    {
        myprintf("Cyw43 scan_start failed");
        error_handler();
    }

    while (1)
    {
        int ret = event_poll();

        if (ret < 0) break;
        if (ret > 0) continue;
        std::this_thread::sleep_for(10ms);
    }
#endif

    // we join inside netInit
    if (!join_start(wfSsid, wfPw))
    {
        myprintf("Cyw43 join_start failed");
        error_handler();
    }

    netTimer.setup(10'000'000);
    while (not netTimer.getUnblocked()[netTimer.id()])
    {
        int ret = event_poll();

        if (ret < 0) break;
        if (ret > 0) continue;
        if (link_check() > 0) break;
        std::this_thread::sleep_for(10ms);
    }

    if (link_check() <= 0)
    {
        if (link_check() == -1)
        {
            myprintf("Join failed!");
        }
        else
        {
            myprintf("Join timeout");
        }
        error_handler();
    }
    myprintf("Joined wifi");
}
