#ifndef ARCH_CC_H_INCLUDED
#define ARCH_CC_H_INCLUDED

#include <stdint.h>
#include <stddef.h>
#include <assert.h>

#ifdef __cplusplus
extern "C" {
#endif

void myprintf(char const *fmt);
#define printf(fmt, ...) (myprintf(fmt))
void fatal(char const *);
#if 0
void myAssertImpl(char const *msg, int line, char const *file);
#define myassert(msg) (myAssertImpl(msg, __LINE__, __FILE__))
void abort(void);
struct _iobuf;
typedef struct _iobuf FILE;
int fflush(FILE *stream);
#endif

#define LWIP_ERRNO_INCLUDE <errno.h>
//#define LWIP_PROVIDE_ERRNO

#ifndef BYTE_ORDER
#define BYTE_ORDER LITTLE_ENDIAN
#endif

typedef uint8_t u8_t;
typedef int8_t s8_t;
typedef uint16_t u16_t;
typedef int16_t s16_t;
typedef uint32_t u32_t;
typedef int32_t s32_t;

typedef uintptr_t mem_ptr_t;
typedef u32_t sys_prot_t;

/* Define (sn)printf formatters for these lwIP types */
// we don't have (sn)printf
#if 0
#define X8_F  "02x"
#define U16_F "hu"
#define S16_F "hd"
#define X16_F "hx"
#define U32_F "lu"
#define S32_F "ld"
#define X32_F "lx"
#define SZT_F U32_F
#endif

/* Compiler hints for packing structures */
#define PACK_STRUCT_FIELD(x)    x
#define PACK_STRUCT_STRUCT  __attribute__((packed))
#define PACK_STRUCT_BEGIN
#define PACK_STRUCT_END

/* Plaform specific diagnostic output */
#define LWIP_PLATFORM_DIAG(x)   printf x
#define LWIP_PLATFORM_ASSERT(x) assert(x)
// 'handler' may contain statements, so we'd need 'do ... while (0)' here
// but we use our myassert anyway, that doesn't return
#define LWIP_ERROR(message, expression, handler) (void)(expression || (assert(message), 0))

#if 0
/* C runtime functions redefined */
#define snprintf _snprintf

u32_t dns_lookup_external_hosts_file(const char *name);

#define LWIP_RAND() ((u32_t)rand())
#endif

#ifdef __cplusplus
}
#endif
#endif /* ARCH_CC_H_INCLUDED */
