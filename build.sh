#! /bin/sh
set -e

SRCRAW="$( dirname "$0" )"
SRCBASE="$( readlink -f $SRCRAW )"

echo "Building firmware-cyw43..."
rm -rf firmware-cyw43
mkdir firmware-cyw43
cd firmware-cyw43
cmake -D ENV_DIR=$SRCBASE/cmake $SRCBASE/pico-lib/firmware-cyw43
make
cd ..


echo "Building sdk-glue..."
rm -rf sdk-glue
mkdir sdk-glue
cd sdk-glue
cmake -D ENV_DIR=$SRCBASE/cmake $SRCBASE/externals/pico-sdk-glue
make

mkdir -p $SRCBASE/pico-bin
cp sdklib/librp2040sdk.a $SRCBASE/pico-bin
cp i2slib/libi2s.a $SRCBASE/pico-bin
cp picowi/libpicowi.a $SRCBASE/pico-bin
cd ..


APPS=""
APPS="$APPS sync-echo-server"
APPS="$APPS async-echo-server"
APPS="$APPS async-echo-server-coro"
APPS="$APPS sound-net"
APPS="$APPS sound-net-coro"

for app in $APPS
do
    echo "Building $app..."
    rm -rf $app
    mkdir $app
    cd $app
    cmake -D ENV_DIR=$SRCBASE/cmake $SRCBASE/src/$app
    make
    cd ..
done
