# Demo examples for ESE 2023 presentation 'IoT Minimized'

To build, run in a new directory `sh <path-to-here>/build.sh`.

The build was tested with a GCC cross toolchain 13
including libstdc++.

This creates the firmware for the WiFi module and the demo applications:

 - firmware-cyw43/cyw43-firmware.bin: the WiFi firmware
 - sync-echo-server/demo.bin: the synchronous TCP echo server
 - async-echo-server/demo.bin: the asynchronous TCP echo server
 - async-echo-server-coro/demo.bin: the asynchronous TCP echo server
   using coroutines
 - sound-net/demo.bin: the wireless speaker demo
 - sound-net-coro/demo.bin: the wireless speaker demo using coroutines
